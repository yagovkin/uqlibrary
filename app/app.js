angular
    .module('app',['ngMaterial', 'ui.router'])
    .config(config);

function config($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/libraries');
    $stateProvider
        .state('libraries', {
            url: '/libraries',
            templateUrl: 'app/views/libraries.html'
        })
        .state('library-info',{
            url: '/library/{libraryId}',
            templateUrl: 'app/views/library-info.html',
            controller: 'LibraryInfoCtrl as vm',
            params: {'libraryId':null}
        });
};



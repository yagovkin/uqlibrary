(function() {
    'use strict';

    angular.module('app').controller('LibraryInfoCtrl', LibraryInfoCtrl);

    function LibraryInfoCtrl(libraryService, $stateParams, $state) {
        var vm = this;
        vm.openLibraries = openLibraries;

        var libraryId= $stateParams.libraryId;
        vm.computers = [];
        vm.library = {};


        init();

        function init() {
            vm.library = libraryService.getSelctedLibrary();
            getLibrary(libraryId);
        }


        // Read the library by ID from API
        function getLibrary(lid) {
            return libraryService.getLibrary(lid).then(function(data) {
                vm.library = data.locations[0];
                return vm.library;
            });
        }


        // View library list
        function openLibraries()
        {
            $state.go('libraries');
        }

    }
})();
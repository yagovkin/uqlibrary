(function() {
    'use strict';

    angular.module('app').controller('LibrariesCtrl', LibrariesCtrl);

    function LibrariesCtrl(libraryService, $state) {
        var vm = this;

        vm.getAllLibraries = getAllLibraries;
        vm.openLibraryDetails = openLibraryDetails;

        init();

        function init() {
            getAllLibraries();
        }


        // Read all the libraries from API
        function getAllLibraries() {
            return libraryService.getAllLibraries().then(function(data) {
                vm.libraries = data.locations;
                return vm.libraries;
            });
        }


        // View selected library info
        function openLibraryDetails(lib)
        {
            libraryService.setLibrary(lib);
            var stateParams = {'libraryId':lib.lid};
            $state.go('library-info', stateParams);
        }

    }
})();
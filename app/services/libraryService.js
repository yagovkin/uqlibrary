angular
    .module('app')
    .service('libraryService', libraryService);

libraryService.$inject = ['$http','$q'];

function libraryService($http, $q, logger) {
    return {
        getAllLibraries: getAllLibraries,
        getLibrary:getLibrary,

        setLibrary:setLibrary,
        getSelctedLibrary:getSelctedLibrary
    };

    function getAllLibraries() {
        return $http.jsonp('https://app.library.uq.edu.au/api/v2/library_hours/day?callback=JSON_CALLBACK')
            .then(getAllLibrariesSuccess)
            .catch(getAllLibrariesFailed);
    }

    function getLibrary(lid)
    {
        return $http.jsonp('https://app.library.uq.edu.au/api/v2/library_hours/week?lid='+lid+'&callback=JSON_CALLBACK')
            .then(getAllLibrariesSuccess)
            .catch(getAllLibrariesFailed);
    }

    function getAllLibrariesSuccess(data) {
        return data.data;
    }

    function getAllLibrariesFailed(e) {
        var newMessage = 'XHR Failed for getLibrary'
        if (e.data && e.data.description) {
            newMessage = newMessage + '\n' + e.data.description;
        }
        e.data.description = newMessage;
        logger.error(newMessage);
        return $q.reject(e);
    }


    var library_selected = {};
    function getSelctedLibrary() {
        return library_selected;
    }

    function setLibrary(value) {
        library_selected = value;
    }


}

describe('LibrariesCtrl', function () {
    var $controller, LibrariesCtrl;
    var libraryService;

    beforeEach(angular.mock.module('ui.router'));
    beforeEach(angular.mock.module('app'));

    // Inject the $controller to create instances of the controller (LibrariesController) we want to test
    beforeEach(inject(function (_$controller_) {
        $controller = _$controller_;
        LibrariesCtrl = $controller('LibrariesCtrl', {});
    }));

    // Inject the library service
    beforeEach(inject(function (_libraryService_) {
        libraryService = _libraryService_;
    }));


    // Verify our controller exists
    it('Controller should be defined', function () {
        expect(LibrariesCtrl).toBeDefined();
    });

    describe('Test controller functions that make calls to API', function () {
        it('should initialize with a call to libraryService.getAllLibraries()', function () {
            spyOn(libraryService, "getAllLibraries").and.callThrough();
            LibrariesCtrl.getAllLibraries();
            expect(libraryService.getAllLibraries).toHaveBeenCalled();
        });

        it('should initialize with a call to libraryService.getAllLibraries()', function () {
            spyOn(libraryService, "getAllLibraries").and.callThrough();
            LibrariesCtrl.getAllLibraries();
            expect(libraryService.getAllLibraries).toHaveBeenCalled();
        });
    });

    it('should initialize opening of library-info page', inject(function($state){
        spyOn($state, 'go');
        LibrariesCtrl.openLibraryDetails({lid: 3832});

        expect($state.go).toHaveBeenCalled();
        expect($state.go).toHaveBeenCalledWith('library-info', {'libraryId': 3832});
    }));

});
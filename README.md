#Overview and implementation time
The project has basic functionality. The system allows user to browse a list of libraries, search by name and view
each library on individually with opening hours information.
Functionality and test cases are limited as the goal of the project is to illustrate familiarity with AngularJS framework.
Extra functionality would increase amount of code, but not its complexity.
Overall implementation time is 4-6 hours.


#Deployment instructions
1) To install all the application dependencies, run from project location (nodejs must be installed locally)
npm install
2) Karma command line interface should be installed locally
npm install -g karma-cli
3) To run unit tests
karma start


#Optimised development environment
For continuous deployment and integration I would use AWS Bamboo.
After each successful build, if all the unit tests passed automated deployment should be scheduled.


#Optimise applications on server
Application could run in Docker container, with all the dependencies injected inside the container.


#Scale applications
Application code should be modularised, logically related controllers, services and directives files should be
"grouped together" and shipped as modules.
From my experience scaling AngularJS applications could be tricky. As application grows, number of
libraries it depends on grows dramatically and each library update is risky for the entire system.
Also angular 1.5 and 2.0 do not work very well together at this stage, so there are two options:
a) keep developing in 1.5 b) re-write the whole app on 2.0.
Another problem is speed, relatively large angular 1.5 applications tend to be very slow.
As a solution ReactJS could be used instead.